/*******************************************************************************
 * MIT License
 *
 * Copyright (c) 2017 Dennis van der Wals
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/
package de.etern.it.mp3shuffle;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JFileChooser;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

/**
 *
 */

/**
 * Sorgt für eine zufüllige Auflistung aller Lieder in einem Verzeichnis, indem
 * die ursprüngliche Nummerierung durch eine zufüllige ersetzt wird.
 *
 * @author vanderwals
 *
 */
public class ShuffleMain {
	private static final String PATTERN_IN = "\\d+ - ", FORMAT_OUT = "%03d - %s", EXTENSION_TO_MANIPULATE = ".mp3",
			EXTENSION_TO_DELETE = ".jpg";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		new ShuffleMain();
	}

	/**
	 * üffnet ein Verzeichnisauswahlfenster. Startet die Umbenennung
	 */
	public ShuffleMain() {
		JFileChooser jf = new JFileChooser();
		jf.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

		int accept = jf.showOpenDialog(null);

		if ((accept == JFileChooser.APPROVE_OPTION) && jf.getSelectedFile().isDirectory()) {
			final File dir = jf.getSelectedFile();

			deleteSubFiles(dir, "._", ".m4a");
		}
	}

	/**
	 * Gibt eine Liste mit der Position aller Lieder in dem Vereichnis zurück -
	 * Also eine Liste von 0 bis #AnzahlLieder
	 *
	 * @param dir
	 *            Verzeichnis der Lieder
	 * @return liste mit Lieder positionen
	 */
	private ArrayList<Integer> countFiles(File dir) {
		if (!dir.isDirectory())
			return null;

		int counter = 0;
		ArrayList<Integer> numbers = new ArrayList<>();

		// Zaehle Lieder
		for (String file : dir.list())
			if (file.endsWith(EXTENSION_TO_MANIPULATE)) {
				numbers.add(counter++);
			}

		return numbers;
	}

	/**
	 * Bennent alle Dateien um und lüscht Albumbilder
	 */
	private void deleteSubFiles(File dir, String toDeleteStart, String toDeleteEnd) {

		for (String file : dir.list()) {
			File f = new File(dir.getAbsolutePath() + "/" + file);

			// Lösche Bilder
			if (file.startsWith(toDeleteStart) || file.endsWith(toDeleteEnd)) {
				f.delete();
				System.out.println("DELETE: " + f.getAbsolutePath());

			} else if (f.isDirectory()) {

				deleteSubFiles(f, toDeleteStart, toDeleteEnd);
			}
		}
	}

	/**
	 * Bestimmt den lüngsten Dateinamen aller Lieder
	 *
	 * @param dir
	 *            Vereichnis der Lieder
	 * @return
	 */
	private int getLongestFileName(File dir) {
		if (!dir.isDirectory())
			return -1;

		int maxLength = 0;

		// Bestimme längsten Dateinamen
		for (String file : dir.list())
			if (file.endsWith(EXTENSION_TO_MANIPULATE) && (maxLength < file.replaceAll(PATTERN_IN, "").length())) {
				maxLength = file.replaceAll(PATTERN_IN, "").length();
			}

		return maxLength;
	}

	/**
	 * Bennent alle Dateien um und lüscht Albumbilder
	 */
	@SuppressWarnings("unused")
	private void renameSubFiles(File dir, final JProgressBar progress) {
		/*
		 * Bereite pad für Dateinamenverlängerung für Consolenausgabe vor
		 */
		int length = getLongestFileName(dir);
		String pad = "";
		for (int i = 0; i < length; i++) {
			pad += " ";
		}

		ArrayList<Integer> numbers = countFiles(dir);
		if (numbers == null)
			return;

		// Mische Reichenfolge
		Collections.shuffle(numbers);
		int counter = 0;

		for (String file : dir.list()) {
			// Lüsche Bilder
			if (file.endsWith(EXTENSION_TO_DELETE)) {
				File f = new File(dir.getAbsolutePath() + "/" + file);
				f.delete();
				System.out.println("DELETE: " + f.getAbsolutePath());

			} else if (file.endsWith(EXTENSION_TO_MANIPULATE)) {

				// Bestimme Dateinamen
				File f = new File(dir.getAbsolutePath() + "/" + file);
				String origin = f.getName();
				origin = (origin + pad).substring(0, length);

				// Bilde neuen Namen
				String newName = String.format(FORMAT_OUT, numbers.get(counter++),
						f.getName().replaceAll(PATTERN_IN, "").trim());

				// Ändere Dateinamen
				f.renameTo(new File(dir.getAbsolutePath() + "/" + newName));
				newName = (newName + pad).substring(0, length);

				System.out.println(String.format("RENAME: %1$20s TO %2$20s", origin, newName));

				// Aktualisiere Fortschritt
				final int progressCounter = counter;
				SwingUtilities.invokeLater(() -> progress.setValue(progressCounter));
			}
		}
	}
}
